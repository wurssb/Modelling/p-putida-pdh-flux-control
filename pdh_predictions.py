import cobra
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from pdh_util import (
    apply_partial_knockouts,
    apply_strain_or_condition,
    biomass_reaction,
    check_regulator_limitation,
    growth_conditions,
    modifications,
    pdh_reactions,
    production_strains,
)


def maybe_new_plot(ax):
    if ax is None:
        fig, ax = plt.subplots()
    else:
        fig = ax.get_figure()
    return fig, ax


def plot_limitation(df, ax=None, fig=None):
    new_figure = fig is None
    fig, ax = maybe_new_plot(ax)

    x1, x2 = df.columns.get_level_values(0).unique()

    ax_right = ax.twinx()
    ax.fill_between(
        df.index,
        df.loc[:, pd.IndexSlice[x2, "minimum"]],
        df.loc[:, pd.IndexSlice[x2, "maximum"]],
        alpha=0.25,
        color="blue",
    )
    ax.plot(df.loc[:, pd.IndexSlice[x2, "maximum"]], color="blue", label="Production")
    ax_right.plot(
        df.loc[:, pd.IndexSlice[x1, "maximum"]], color="orange", label="Growth"
    )

    ax_right.set_ylabel("Growth rate")
    ax.set_ylabel("Production rate")
    ax.set_xlabel("PDH flux")
    ax.set_ylim(0)
    ax_right.set_ylim(0)
    ax.set_xlim(df.index.min(), df.index.max())
    if new_figure == "new":
        fig.legend(frameon=False)
        sns.despine(fig, right=False)


def plot_limitations_together(data):
    df = pd.concat({k: v.iloc[:, 2] for k, v in data.items()}, axis=1)
    biomass = data["Pyruvate"].iloc[:, 0]

    fig, ax_left = plt.subplots()
    ax_right = ax_left.twinx()

    for metabolite in df.columns:
        ax_left.plot(df.loc[:, metabolite], label=metabolite, linestyle="dashed")
    ax_right.plot(biomass, color="orange", label="Growth")

    ax_right.set_ylabel("Growth rate")
    ax_left.set_ylabel("Production rate")
    ax_left.set_xlabel("PDH flux")
    ax_left.set_ylim(0)
    ax_right.set_ylim(0, biomass.max() * 1.05)
    ax_left.set_xlim(df.index.min(), df.index.max())

    sns.despine(fig, right=False)
    fig.tight_layout()
    fig.legend(frameon=False, loc="center right")
    return fig, ax_left, ax_right


if __name__ == "__main__":
    plt.ion()

    model = cobra.io.read_sbml_model("./data/models/Nogales2019/Bigg_iJN1463.xml")
    # original_model = model.copy()

    # Verify the essentiality in glucose, fructose and glycerol.
    with model:
        model.reactions.EX_glc__D_e.lower_bound = 0
        for carbon_source, condition in growth_conditions.items():
            with model:
                apply_strain_or_condition(model, condition)
                # Growth after PDH KO
                normal = model.slim_optimize()
                print(
                    "Carbon source: {:>8} - normal growth {:.2f}".format(
                        carbon_source, normal
                    )
                )

        apply_partial_knockouts(model, modifications)
        for carbon_source, condition in growth_conditions.items():
            with model:
                apply_strain_or_condition(model, condition)

                # Growth after PDH KO
                before = model.slim_optimize()

                # Growth before PDH KO
                with model:
                    for reaction in pdh_reactions:
                        model.reactions.get_by_id(reaction).knock_out()
                    after = model.slim_optimize()

                    # Check with acetate suplemenation, this should grow again.
                    with model:
                        model.reactions.EX_ac_e.lower_bound = -1
                        acetate = model.slim_optimize()

                print(
                    "ko-strain with PDH active vs inactive vs acetate supplemented"
                    " on {:>8}: {:.2f} | {:.2f} | {:.2f}".format(
                        carbon_source, before, after, acetate
                    )
                )

    # Check PDH limitation: product vs biomass flux
    regulator = "PDH"
    data = {}
    model.objective = biomass_reaction
    for strain, strain_info in production_strains.items():
        with model:
            # Prevent unrealistic uptake of CO2 for pyruvate production.
            model.reactions.EX_co2_e.lower_bound = 0
            # model.reactions.ATPM.bounds = (0, 0)
            for reaction in pdh_reactions:
                model.reactions.get_by_id(reaction).knock_out()
            # Added changes to make PDH essential
            apply_partial_knockouts(model, modifications)
            apply_strain_or_condition(model, strain_info)

            # Calculate growth - production trade-off.
            data[strain] = check_regulator_limitation(
                model,
                regulator,
                strain_info["objectives"],
                steps=np.linspace(0, 2.5, num=100),
            )
    pd.concat(data, axis=1).to_excel('results/simulation_data.xlsx')

    fig, axes = plt.subplots(2, 3)
    for (metabolite, df), ax in zip(data.items(), axes.flat):
        plot_limitation(df, ax)
        ax.set_title(metabolite)

    # axes.flat[-1].set_visible(False)
    sns.despine(fig, right=False)
    fig.tight_layout()
    fig.savefig("results/products_seperate.png", dpi=300)

    fig, axl, axr = plot_limitations_together(data)
    fig.savefig("results/products_combined.png", dpi=300)
