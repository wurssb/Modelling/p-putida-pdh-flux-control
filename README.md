# PDH Flux control
Analysis code for the publication "Modulation of the PDH reaction enables dynamic growth control and increased product formation in *P. putida* KT2440"

## Contents:
- `pdh_essentiality.py` contains the code used to find combinations of reactions knock-outs leading to the PDH essentiality phenotype as is experimentally observed.
- `pdh_predictions.py` contains the code used to simulate the usage of the PDH reaction as a metabolic valve.
- `pdh_util.py` contains a number of shared utility functions.
- `data/` contains the iJN1463 model used and a number of data sets used to evaluate the presence of reactions.
- `results/` contains all results
    - `ko_strains.txt` - data file containing the combinations of reaction knock-outs found.
    - `combinations.xlsx` - Analysed combinations and evidence for reaction presence.
    - `products*.png` - Growth / production trade-off when PDH flux is limited for different products.

## Requirements:
Code was developed and tested with the following versions:
- [Python](https://python.org) - Used version: `3.7.9`
    -  [pandas](https://pandas.pydata.org/) - Used version: `1.1.4`
    -  [cobrapy](https://github.com/opencobra/cobrapy) - Used version: `0.19.0`
    -  [gurobipy](https://www.gurobi.com/) - Academic licenses are available (Used version `9.0.3`)
    -  [biopython](https://biopython.org/) - Used version: `1.78`
    -  [goatools](https://github.com/tanghaibao/goatools/) - Used version: `1.0.15 `
    -  [numpy](https://www.numpy.org/) - Used version: `1.19.1`
    -  [matplotlib](https://matplotlib.org/) - Used version: `3.3.1`
    -  [seaborn](https://seaborn.pydata.org/) - Used version: `0.11.0`
    -  [networkx](https://networkx.org/) - Used version: `2.5`

## Authors:
- Rik van Rosmalen

## License:
This project is licensed under the MIT License - see the LICENSE file for details, with the following exceptions:
- `print_as_tree` in `pdh_util.py` is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0)
- everything under the `data/` directory.