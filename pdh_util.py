import collections
import itertools

import cobra
import networkx as nx
import pandas as pd


def print_as_tree(branches):
    """Print a list of branches as a tree.

    Adapted from: https://stackoverflow.com/a/56642198 by user "hiro protagonist"
    under CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0)
    """
    try:
        import anytree
    except ImportError:
        raise ImportError("Install `anytree` for tree printing functionality.")

    # Since we don't care about order, we can sort in whatever way we like.
    # Thus we make sure the parents are always the genes most common
    order = collections.Counter(itertools.chain.from_iterable(branches))
    branches = [
        sorted(branch, key=lambda x: (order[x], x), reverse=True) for branch in branches
    ]

    root_node = anytree.Node("*")
    for branch in branches:
        parent_node = root_node
        for cur_node_name in branch:
            cur_node = next(
                (node for node in parent_node.children if node.name == cur_node_name),
                None,
            )
            if cur_node is None:
                cur_node = anytree.Node(cur_node_name, parent=parent_node)
            parent_node = cur_node

    for pre, _, node in anytree.RenderTree(root_node):
        print("{}{}".format(pre, node.name))


def to_networkx(
    model, type="bipartite", ignore_reactions=None, ignore_metabolites=None
):
    """Convert a cobrapy model to a networkX graph.

    Types:
        - bipartite: There are two sets of nodes, nodes and reactions.
            edge(m -> r | s) -> consumption of (s) units of metabolite (m) in reaction (r)
            edge(r -> m | s) -> production of (s) units of metabolite (m) in reaction (r)
        - metabolites: Nodes are metabolites, linked by shared reactions.
            edge(m1 -> m2 | r) -> reaction (r) with m1 + ... -> m2 + ...
        - reactions: Nodes are reactions, linked by shared metabolites.
            edge(r1 -> r2 | m) -> reaction r1 ... -> m and reaction r2 m -> ...
            Warning: Can be very large!

    Reactions and metabolites can be excluded by passing a list of ids to
    "ignore_reactions" or "ignore_metabolite".
    """
    # S = cobra.util.create_stoichiometric_matrix(model, "DataFrame")
    # if ignore_reactions is not None:
    #     S = S.loc[:, ~S.columns.isin(ignore_reactions)]
    # if ignore_metabolites is not None:
    #     S = S.loc[~S.index.isin(ignore_metabolites), :]

    if ignore_metabolites is None:
        ignore_metabolites = []
    if ignore_reactions is None:
        ignore_reactions = []

    if type == "bipartite":
        edges = []
        for reaction in model.reactions:
            if reaction.id in ignore_reactions:
                continue
            for met, s in reaction.metabolites.items():
                if met.id in ignore_metabolites:
                    continue
                if s > 0 or reaction.reversibility:
                    edges.append((reaction.id, met.id, {"stoichiometry": s}))
                if s < 0 or reaction.reversibility:
                    edges.append((met.id, reaction.id, {"stoichiometry": abs(s)}))
        G = nx.from_edgelist(edges, create_using=nx.DiGraph)
        G.add_nodes_from(m.id for m in model.metabolites if not m.reactions)
    elif type == "metabolite":
        edges = []
        for reaction in model.reactions:
            if reaction.id in ignore_reactions:
                continue
            edges.extend(
                itertools.product(
                    (
                        m.id
                        for m in reaction.reactants
                        if m.id not in ignore_metabolites
                    ),
                    (m.id for m in reaction.products if m.id not in ignore_metabolites),
                    ({"reaction": reaction.id},),
                )
            )
            if reaction.reversibility:
                edges.extend(
                    itertools.product(
                        (
                            m.id
                            for m in reaction.products
                            if m.id not in ignore_metabolites
                        ),
                        (
                            m.id
                            for m in reaction.reactants
                            if m.id not in ignore_metabolites
                        ),
                        ({"reaction": reaction.id},),
                    )
                )
        G = nx.from_edgelist(edges, create_using=nx.MultiDiGraph)
    elif type == "reaction":
        edges = []
        for metabolite in model.metabolites:
            if metabolite.id in ignore_metabolites:
                continue
            for r1, r2 in itertools.combinations(
                [r for r in model.reactions if r not in ignore_reactions], r=2
            ):
                if (metabolite in r1.products or r1.reversibility) and (
                    metabolite in r2.reactants or r2.reversibility
                ):
                    edges.append((r1.id, r2.id, {"metabolite": metabolite.id}))
                if (metabolite in r2.products or r2.reversibility) and (
                    metabolite in r1.reactants or r1.reversibility
                ):
                    edges.append((r2.id, r1.id, {"metabolite": metabolite.id}))
        G = nx.from_edgelist(edges, create_using=nx.MultiDiGraph)
    else:
        raise ValueError("Invalid network type.")
    return G


def ko_reaction_per_direction(reaction, direction):
    """Apply partial or full knockout depending on the direction."""
    old_bounds = reaction.bounds
    if direction == "both":
        reaction.bounds = (0, 0)
    elif direction == "forward":
        reaction.upper_bound = 0
    elif direction == "reverse":
        reaction.lower_bound = 0
    else:
        raise ValueError("Invalid direction: {}".format(direction))
    return old_bounds


def apply_partial_knockouts(model, knockouts):
    """Apply a set of partial knockouts to the model."""
    for reaction, direction in knockouts:
        reaction = model.reactions.get_by_id(reaction)
        ko_reaction_per_direction(reaction, direction)


def apply_strain_or_condition(model, strain_info):
    # Add metabolites
    metabolites = []
    for id, name, formula, charge, _ in strain_info["added_metabolites"]:
        metabolites.append(cobra.Metabolite(id, formula, name, charge, compartment="c"))
    if metabolites:
        model.add_metabolites(metabolites)

    # Add reactions
    for id, reaction_string in strain_info["added_reactions"]:
        reaction = cobra.Reaction(id)
        model.add_reactions([reaction])
        reaction.build_reaction_from_string(reaction_string)

    # Add knock-outs
    for gene in strain_info["deleted"]:
        model.genes.get_by_id(gene).knock_out()

    # Apply other constrains
    for reaction, bounds in strain_info["constraints"]:
        model.reactions.get_by_id(reaction).bounds = bounds


def find_essential_reactions(model, strains, threshold_growth, threshold_production):
    essential = set()
    for strain in strains:
        with model:
            apply_strain_or_condition(model, strain)
            for objective in strain["objectives"]:
                model.objective = objective
                threshold = (
                    threshold_growth if "BIOMASS" in objective else threshold_production
                ) * model.slim_optimize()
                essential |= cobra.flux_analysis.find_essential_reactions(
                    model, threshold
                )
    return essential


def check_regulator_limitation(model, regulator, targets, steps):
    data = {}
    for step in steps:
        with model:
            model.reactions.get_by_id(regulator).bounds = (step, step)
            data[step] = cobra.flux_analysis.flux_variability_analysis(
                model, targets, fraction_of_optimum=1.0
            )
    df = pd.concat(data).unstack().swaplevel(axis=1).sort_index(axis=1)
    df.index.name = regulator
    return df


production_strains = {
    "Pyruvate": {
        "constraints": [("EX_glc__D_e", (-6.0, 1000))],
        "over-expressed": [],
        "deleted": [],
        "added_reactions": [],
        "added_metabolites": [],
        "objectives": ["BIOMASS_KT2440_WT3", "EX_pyr_e"],
    },
    "2-Ketoisovalerate": {  # Also know as: 3-Methyl-2-oxobutanoate or alpha-keto-isovalerate
        "constraints": [("EX_glc__D_e", (-6.0, 1000))],
        "over-expressed": ["PP_4678", "PP_5128"],  # "alsS", "ilvC", "ilvD"
        "deleted": ["PP_4401"],  # bkdAA
        "added_reactions": [("EX_3mob_c", "3mob_c => ")],
        "added_metabolites": [],
        "objectives": ["BIOMASS_KT2440_WT3", "EX_3mob_c"],
    },
    "L-Alanine": {
        "constraints": [("EX_glc__D_e", (-6.0, 1000))],
        "over-expressed": [],
        "deleted": ["PP_1872"],  # alaA
        "added_reactions": [
            ("alaD", "ala__L_c + nad_c + h2o_c <=> pyr_c + nh4_c + nadh_c + h_c"),
            # Excisting export only goes from e -> p -> c and not in reverse.
            ("EX_ala__L_c", "ala__L_c => "),
        ],
        "added_metabolites": [],
        "objectives": ["BIOMASS_KT2440_WT3", "EX_ala__L_c"],
    },
    "Lactate": {
        "constraints": [("EX_glc__D_e", (-6.0, 1000))],
        "over-expressed": [],
        "deleted": [],
        "added_reactions": [],
        "added_metabolites": [],
        "objectives": ["BIOMASS_KT2440_WT3", "EX_lac__D_e"],
    },
    "Lycopene": {
        "constraints": [("EX_glc__D_e", (-6.0, 1000))],
        "over-expressed": ["PP_0527"],  # dxs
        "deleted": [],
        "added_reactions": [
            ("crtE", "frdp_c + ipdp_c <=> ggdp_c + ppi_c"),
            ("crtB", "2 ggdp_c <=> cphy_c + 2 ppi_c"),
            # Brenda mentions FAD as co-factor, multistep-reaction.
            # Also mentions that some enyzmes producee intermediate desaturated products.
            ("crtI", "cphy_c + 4 fad_c <=> 4 fadh2_c + lyco_c"),
            ("EX_lyco_c", "lyco_c => "),
        ],
        "added_metabolites": [
            (
                "ggdp_c",
                "(2E,6E,10E)-geranylgeranyl diphosphate",
                "C20H33O7P2",
                -3,
                {"inchi_key": "OINNEUNVOZHBOX-QIRCYJPOSA-K"},
            ),
            (
                "cphy_c",
                "15-cis-phytoene",
                "C40H64",
                0,
                {"inchi_key": "YVLPJIGOMTXXLP-BHLJUDRVSA-N"},
            ),
            (
                "lyco_c",
                "all-trans-lycopene",
                "C40H56",
                0,
                {"inchi_key": "OAIJSZIZWZSQBC-GYZMGTAESA-N"},
            ),
        ],
        "objectives": ["BIOMASS_KT2440_WT3", "EX_lyco_c"],
    },
    "Acetaldehyde": {
        "constraints": [("EX_glc__D_e", (-6.0, 1000))],
        "over-expressed": [],
        # Deletes GCALDD, which is essential.
        "deleted": [
            # "PP_2674",
            # "PP_2680",
            # "PP_2679",
            # "PP_0545",
        ],  # qedH-I, qedH-II, aldB-I, aldB-II
        "added_reactions": [("pdc", "pyr_c + h_c => acald_c + co2_c")],
        "added_metabolites": [],
        "objectives": ["BIOMASS_KT2440_WT3", "EX_acald_e"],
    },
}


growth_conditions = {
    "glucose": {
        "constraints": [("EX_glc__D_e", (-6, 1000))],
        "over-expressed": [],
        "deleted": [],
        "added_reactions": [],
        "added_metabolites": [],
        "objectives": ["BIOMASS_KT2440_WT3"],
    },
    "fructose": {
        "constraints": [("EX_glc__D_e", (0, 1000)), ("EX_fru_e", (-6, 1000))],
        "over-expressed": [],
        "deleted": [],
        "added_reactions": [],
        "added_metabolites": [],
        "objectives": ["BIOMASS_KT2440_WT3"],
    },
    "glycerol": {
        "constraints": [("EX_glc__D_e", (0, 1000)), ("EX_glyc_e", (-6, 1000))],
        "over-expressed": [],
        "deleted": [],
        "added_reactions": [],
        "added_metabolites": [],
        "objectives": ["BIOMASS_KT2440_WT3"],
    },
}

biomass_reaction = "BIOMASS_KT2440_WT3"

pdh_reactions = ("PDH", "PDHa", "PDHbr")

# Smallest modification possible to make PDH essential, as is experimentally observed.
# We pick one of the knock-out sets here to get the PDH required phenotype.
# Most only need to be knocked-out in one direction.
modifications = (
    ("ALDD2x", "forward"),
    ("MACCOAT", "forward"),
    ("MMSAD3", "forward"),
    ("ACACT1r", "reverse"),
    ("ACALD", "forward"),
    ("3OXCOAT", "forward"),
    ("ALDD2y", "forward"),
    ("KAT1", "forward"),
)
