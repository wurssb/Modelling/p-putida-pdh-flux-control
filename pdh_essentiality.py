import collections
import heapq
import itertools
import json
import pathlib
import re

import cobra
import networkx as nx
import numpy as np
import pandas as pd
from Bio import SeqIO
from goatools.gosubdag.gosubdag import GoSubDag
from goatools.obo_parser import GODag

from pdh_util import (
    biomass_reaction,
    find_essential_reactions,
    growth_conditions,
    ko_reaction_per_direction,
    pdh_reactions,
    print_as_tree,
    production_strains,
    to_networkx,
)


def get_metabolic_genes(go_annotation_path, go_graph_path):
    """Check which genes are involved in metabolic processes (GO:0008152)."""
    go_graph = GODag(go_graph_path, prt=False)

    def is_metabolic_process(go_terms):
        if not go_terms or str(go_terms) == "nan":
            return False
        go_terms = set(go_terms.split("; "))
        sub_graph = GoSubDag(go_terms, go_graph, prt=False)
        if not sub_graph.go_sources:
            return False
        return any(
            "GO:0008152" in set(sub_graph.rcntobj.go2ancestors[go_term])
            for go_term in sub_graph.go_sources
        )

    df = pd.read_csv(go_annotation_path, sep="\t")
    df.index = df["Gene names"].str.extract("(PP_\d{4})")[0]
    df = df["Gene ontology IDs"].apply(is_metabolic_process)
    df.index.name = "gene"
    df.name = "metabolic"
    return df


def get_reaction_and_direction(reaction):
    """Extract the reaction id and direction from reactions formatted as reaction(-1|1)"""
    # Word (letter / number / underscored ended by optional 1 or -1 in parenthesis.
    match = re.match(r"(\w+)(?:\((-1|1)\))?$", reaction)
    if match is None:
        raise ValueError("Couldn't match reaction")
    reaction_id, direction = match.groups()
    if direction is None:
        direction = "both"
    elif direction == "1":
        direction = "forward"
    elif direction == "-1":
        direction = "reverse"
    else:
        raise ValueError("Invalid direction")
    return reaction_id, direction


def verify_in_full_model(
    model, ko_reactions, pdh_reactions=("PDH", "PDHa", "PDHbr"), skipref=False,
):
    """Verify the growth rates with and without the pdh reactions active."""
    pdh_reactions = [get_reaction_and_direction(i) for i in pdh_reactions]
    ko_reactions = [get_reaction_and_direction(i) for i in ko_reactions]
    objective = list(cobra.util.linear_reaction_coefficients(model).keys())

    # Check objective with full model.
    with model:
        if not skipref:
            sol = model.optimize()
            ref = sum(sol.get_primal_by_id(i.id) for i in objective)
        for reaction, direction in pdh_reactions:
            ko_reaction_per_direction(model.reactions.get_by_id(reaction), direction)
        if not skipref:
            sol = model.optimize()
            pdh_ko = sum(sol.get_primal_by_id(i.id) for i in objective)

        # Check objective with no pdh + ko applied
        for reaction, direction in ko_reactions:
            ko_reaction_per_direction(model.reactions.get_by_id(reaction), direction)
        sol = model.optimize()
        ko = sum(sol.get_primal_by_id(i.id) for i in objective)

    with model:
        # Add back PDH
        for reaction, direction in ko_reactions:
            ko_reaction_per_direction(model.reactions.get_by_id(reaction), direction)
        # Check objective
        sol = model.optimize()
        pdh_repair = sum(sol.get_primal_by_id(i.id) for i in objective)

    if skipref:
        return ko, pdh_repair
    else:
        return ref, pdh_ko, ko, pdh_repair


def augment_solution_with_duplicates(
    model, duplicate_reactions, strain, biomass_threshold=0.1
):
    """Find a minimal solution for the model without deletions based on an existing strain."""
    strain_with_direction = [get_reaction_and_direction(i) for i in strain]

    extras = []
    for reaction, _ in strain_with_direction:
        for extra in duplicate_reactions.get(reaction, []):
            if model.reactions.get_by_id(extra).reversibility:
                extras.append("{}(1)".format(extra))
                extras.append("{}(-1)".format(extra))
            else:
                extras.append("{}(1)".format(extra))

    if not extras:
        return strain

    with model:
        # Set base strain
        for reaction, direction in strain_with_direction:
            reaction = model.reactions.get_by_id(reaction)
            ko_reaction_per_direction(reaction, direction)

        # Generate all possibilities, starting from the smallest.
        combinations = itertools.chain.from_iterable(
            itertools.combinations(extras, r=i) for i in range(len(extras) + 1)
        )
        for subset in combinations:
            # Check if sufficient
            ko, repair = verify_in_full_model(model, subset, skipref=True)
            if ko < biomass_threshold and repair > biomass_threshold:
                return strain + list(subset)
    return None


def find_similar_reactions(model, ignore=None):
    """Find similar reactions, ignoring co-factors or directionality."""
    if ignore is None:
        ignore = {
            "nad_c",
            "nadh_c",
            "nadp_c",
            "nadph_c",
            "fad_c",
            "fadh2_c",
            "atp_c",
            "adp_c",
            "amp_c",
            "q8_c",
            "q8h2_c",
            "h_c",
            "h2o_c",
        }
    seen = {}
    for reaction in model.reactions:
        # Remove co-factors
        metabolites = {
            k.id: v for k, v in reaction.metabolites.items() if k.id not in ignore
        }

        # Normalize direction
        if sorted(k for k, v in metabolites.items() if v > 0) > sorted(
            k for k, v in metabolites.items() if v < 0
        ):
            metabolites = {k: -v for k, v in metabolites.items()}

        # Generate hash
        hash_key = hash(tuple(sorted(metabolites.items())))

        # Check if seen before
        if hash_key in seen:
            yield seen[hash_key], reaction.id
        else:
            seen[hash_key] = reaction.id


def update_model(model, reactions_to_remove):
    """Update the model to simplify the reaction search."""
    model = model.copy()
    model.id = model.id + "_simplified"
    # Remove maintenance.
    model.reactions.ATPM.bounds = 0, 0

    # Accoa export
    accoa_c_export = cobra.Reaction("EX_accoa_c")
    accoa_c_export.add_metabolites(
        {
            model.metabolites.accoa_c: -1,
            model.metabolites.o2_c: -2,
            model.metabolites.co2_c: 2,
            model.metabolites.h2o_c: 1,
            model.metabolites.coa_c: 1,
        }
    )
    # Provide free ATP / NADH / NADPH / FAD / Q8
    # This will avoid the activation of pathways just to generate co-factors.
    atp = cobra.Reaction("free_atp", lower_bound=0, upper_bound=0)
    atp.add_metabolites(
        {
            model.metabolites.adp_c: -1,
            model.metabolites.pi_c: -1,
            model.metabolites.h_c: -1,
            model.metabolites.atp_c: 1,
            model.metabolites.h2o_c: 1,
        }
    )
    nad = cobra.Reaction("free_nadh", lower_bound=0, upper_bound=0)
    nad.add_metabolites(
        {
            model.metabolites.nad_c: -1,
            model.metabolites.h_c: -1,
            model.metabolites.nadh_c: 1,
        }
    )
    nadp = cobra.Reaction("free_nadp", lower_bound=0, upper_bound=0)
    nadp.add_metabolites(
        {
            model.metabolites.nadp_c: -1,
            model.metabolites.h_c: -1,
            model.metabolites.nadph_c: 1,
        }
    )
    fad = cobra.Reaction("free_fad", lower_bound=0, upper_bound=0)
    fad.add_metabolites(
        {
            model.metabolites.fad_c: -1,
            model.metabolites.h_c: -2,
            model.metabolites.fadh2_c: 1,
        }
    )
    q8 = cobra.Reaction("free_q8", lower_bound=0, upper_bound=0)
    q8.add_metabolites(
        {
            model.metabolites.q8_c: -1,
            model.metabolites.h_c: -2,
            model.metabolites.q8h2_c: 1,
        }
    )

    free_cofactors = [atp, nad, nadp, fad, q8]
    model.add_reactions(free_cofactors + [accoa_c_export])

    model.remove_reactions(
        reactions_to_remove, remove_orphans=False,
    )
    return model, free_cofactors


def get_graph_distance_to(model, node, ignore_metabolites, undirected=False):
    """Get the distance to a reaction/metabolite based on a (un)directed bipartite graph."""
    graph = to_networkx(model, "bipartite", None, ignore_metabolites)
    if undirected:
        graph = graph.to_undirected()
    return dict(nx.single_target_shortest_path_length(graph, node))


def check_essentiality(
    model,
    objective_reaction,
    biomass_reaction,
    essential_reaction,
    free_cofactors,
    reactions,
    objective_threshold=1,
    biomass_threshold=0.1,
    flux_threshold=1e-6,
):
    # Verify if growth is possible with the essential reaction knocked-out.
    with model:
        essential_reaction.bounds = 0, 0
        model.objective = biomass_reaction
        grows_without_essential = model.slim_optimize()
        grows_without_essential = (
            grows_without_essential and biomass_reaction.flux >= biomass_threshold
        )
    active_reactions = None

    # Reaction k.o does not prevent growth?
    # -> We need to find more reactions to remove.
    if grows_without_essential:
        # We do this on the "simplified" model, this model is modified
        # to have fewer reactions changing by adding free co-factors.
        with model:
            # Set free co-factors
            for reaction in free_cofactors:
                reaction.bounds = -1000, 1000
            # Solve for the "simplified" objective using pFBA to minimize total flux,
            # which hopefully minimizes active reactions as well.
            fluxes = cobra.flux_analysis.pfba(
                model, 1.0, objective_reaction, reactions + [objective_reaction]
            ).fluxes
            # Enough flux through simplifed objective?
            # -> Yes, continue searching using these active reactions as targets
            if fluxes.loc[objective_reaction.id] >= objective_threshold:
                active = fluxes.abs() > flux_threshold
                active_reactions = set(
                    np.sign(fluxes.loc[active]).astype(int).items()
                ) - {(objective_reaction.id, 1)}
                status = "continue"
            # -> No. This is a dead-end, stop searching using this strain.
            else:
                status = "stop - no production"
    # Reaction k.o does prevent growth?
    # -> Verify that it does growth with the reaction active.
    else:
        with model:
            essential_reaction.bounds = -1000, 1000
            model.objective = biomass_reaction
            grows_with_essential = model.slim_optimize()
            grows_with_essential = (
                grows_with_essential and biomass_reaction.flux >= biomass_threshold
            )
        # Does it grow now with our "essential" reaction active?
        # -> Yes. We have the phenotype we want.
        if grows_with_essential:
            status = "hit"
        # -> No. This is a dead-end, stop searching using this strain.
        else:
            status = "stop - no growth recovery"

    return status, active_reactions


def find_required_ko(
    model,
    objective_reaction,
    biomass_reaction,
    essential_reaction,
    starting_strain,
    allowed_reactions,
    free_cofactor_reactions,
    reaction_cost,
    results_path,
    max_length,
    length_penalty,
    objective_threshold=1,
    biomass_threshold=0.1,
    flux_threshold=1e-6,
    verbose=False,
):
    starting_strain = frozenset(starting_strain)

    status, active = check_essentiality(
        model,
        objective_reaction,
        biomass_reaction,
        essential_reaction,
        free_cofactor_reactions,
        allowed_reactions,
        objective_threshold=objective_threshold,
        biomass_threshold=biomass_threshold,
        flux_threshold=flux_threshold,
    )

    if status != "continue":
        raise ValueError("Invalid starting phenotype.")

    seen = set()
    explored = 0
    success = []
    # Queue layout: (cost, length, iteration, strain, active)
    queue = []
    heapq.heappush(queue, (0, len(starting_strain), explored, starting_strain, set()))

    while queue:
        cost, length, _, strain, previous_active_reactions = heapq.heappop(queue)
        explored += 1

        if verbose:
            print("Strain: {}".format(strain - starting_strain))

        # Check if essential or check active reactions.
        with model:
            for (reaction, direction) in strain:
                reaction = model.reactions.get_by_id(reaction)
                if direction == 1:
                    reaction.upper_bound = 0
                elif direction == -1:
                    reaction.lower_bound = 0
            status, active = check_essentiality(
                model,
                objective_reaction,
                biomass_reaction,
                essential_reaction,
                free_cofactor_reactions,
                allowed_reactions,
                objective_threshold=objective_threshold,
                biomass_threshold=biomass_threshold,
                flux_threshold=flux_threshold,
            )

        if verbose:
            print("Status: {}".format(status))

        if status == "continue":
            new_reactions = active - previous_active_reactions
            if verbose:
                print("Newly active reactions: {}".format(len(new_reactions)))

            # Extra penalty for very long solutions
            if len(new_reactions) + length > max_length:
                penalty = len(new_reactions) * length_penalty
            else:
                penalty = 0

            # Add all new possibilities
            for reaction, direction in new_reactions:
                new_strain = strain.union({(reaction, direction)})
                # Skip:
                #   - anything already seen
                #   - anything that is too long.
                #   - anything that is a superset of one of the current solutions.
                if (
                    new_strain not in seen
                    and len(new_strain) < max_length
                    and not any(new_strain > strain for strain in success)
                ):
                    heapq.heappush(
                        queue,
                        (
                            # New cost + cost for the reaction + total set based size penalty
                            cost + reaction_cost[reaction] + penalty,
                            length + 1,
                            explored,
                            new_strain,
                            active,
                        ),
                    )
                    # Add the strain to be skipped in the future.
                    # We already add it to seen since it's queued and we don't want
                    # to add it again in the period before we actually run it.
                    seen.add(new_strain)
            if verbose:
                print(
                    "Explored: {} | Queue: {} | Solutions: {} | cost: {:.1f} | length: {}".format(
                        explored, len(queue), len(success), cost, length
                    )
                )
        # Got a success? Save it
        elif status == "hit":
            with open(results_path, "a") as outfile:
                outfile.write(
                    ", ".join("{}({})".format(*i) for i in strain - starting_strain)
                    + "\n"
                )
            success.append(strain)
        elif status.startswith("stop"):
            continue
    return success


def get_nogales_data(data_path, mapping_path):
    confidence = pd.read_excel(
        data_path, sheet_name="Reaction Confidence Score", index_col=0,
    ).iloc[:-1, [1]]
    subsystem = pd.read_excel(
        data_path, sheet_name="Reactions in iJN1411", index_col=0
    ).Subsystem

    with open(mapping_path, "r") as infile:
        mapping = json.load(infile)
    reaction_mapping = mapping["reactions"][1]

    result = []
    for series in (confidence, subsystem):
        series.index = series.index.str.replace(
            r"[\(\)-]", "_", regex=True
        ).str.replace(r",", "_", regex=True)

        series = series.rename(index=reaction_mapping).reindex(
            pd.Index(reaction_mapping.values())
        )
        result.append(series)

    confidence, subsystem = result

    return confidence.fillna(0).astype(int), subsystem.fillna("Unannotated").astype(str)


def load_transcriptomics_data_kim2013(data_path, genome_path):
    record = SeqIO.read(genome_path, "genbank")

    data = []
    for filename in data_path.glob("GSM*RPKM.txt"):
        df = pd.read_csv(filename, sep="\t").iloc[:, :2]
        df = (
            df.assign(location=df.GeneID.str.split(":", expand=True).iloc[:, 1])
            .drop("GeneID", axis=1)
            .set_index("location")
        )
        df.columns = [df.columns[0].split("_")[1]]
        data.append(df)
    df = pd.concat(data, axis=1).reset_index()

    genes = {}
    locations = {}
    for location in df.location:
        strand = -1 if location.startswith("c") else 1
        start, end = sorted(int(i) for i in location.strip("c").split("-"))
        locations[(start, end, strand)] = location

    for feature in record.features:
        if feature.type != "gene":
            continue
        key = (
            int(feature.location.start) + 1,
            int(feature.location.end),
            feature.location.strand,
        )
        if key in locations:
            location = locations[key]
            gene = feature.qualifiers.get("gene")
            locus_tag = feature.qualifiers["locus_tag"]
            assert (gene is None or len(gene) == 1) and len(locus_tag) == 1
            genes[location] = (locus_tag[0], "" if gene is None else gene[0])

    assert len(genes) == len(locations)

    df = (
        pd.concat(
            [
                df,
                pd.DataFrame(
                    df.location.map(genes).to_list(), columns=["locus_tag", "gene"]
                ),
            ],
            axis=1,
        )
        .set_index("locus_tag")
        .sort_index()
    )
    return df


def load_transcriptomics_data_sudarsan2014(data_path):
    # Results extraceted from supplementary table, more complete data was found in a link to GEO.
    # data_path = pathlib.Path("data/studies/Sudarsan2014/table_s1_extracted.xlsx")
    # df = pd.read_excel(data_path, header=[0,1], index_col=[0,1,2])
    # df.index = df.index.get_level_values(1).str.replace('PP', 'PP_')

    # GEO: GSE26785
    # Individual normalized signal intensity VALUES for each channel of each array are provided
    data_path = pathlib.Path(
        "data/studies/Sudarsan2014/GSE26785_FullNormalizedMatrix.txt"
    )
    df = pd.read_csv(data_path, sep="\t", header=[1, 2, 3, 4], index_col=[0, 1, 2])
    df.index = df.index.get_level_values(0).str.replace("PP", "PP_")
    df.columns = df.columns.get_level_values(2)
    # There is steady state data and shifting data.
    # Stick to the steady state for now.
    # df = df.loc[:, df.columns.str.contains('steady state')]
    df.columns = df.columns.str.replace("steady state ", "")
    # Data is log2? log10? Normalized?
    df = np.power(10, df)
    # Take mean of replicates
    mean = df.groupby(level=0, axis=1).mean()
    # I can't get to the exact numbers as they are in the supplement...
    # TODO: Only keep metabolic genes

    return mean  # np.log2(mean.T / mean['glucose']).T


def load_transcriptomics_data_d_arrigo2019(data_path):
    # GEO: GSE118554
    all_data = []
    for path in sorted(data_path.glob("*.txt.gz")):
        df = pd.read_csv(path, sep="\t").set_index("Synonym")
        column = df.columns[df.columns.str.contains("RPKM")][0]
        all_data.append(df[column])

    df = pd.concat(all_data, axis=1)
    df.columns = df.columns.str.replace("RPKM ", "")
    # TODO: Only keep metabolic genes
    # df[df.index.str.match('PP_\d{4}')]

    return df.groupby(lambda x: x[:3], axis=1).mean()


def load_essentiality_data_banerjee2020(data_path):
    return pd.read_excel(data_path, skiprows=3).iloc[:-2, :6].set_index("LocusID")


def load_essentiality_data_fitness_browser(data_path):
    return (
        pd.read_csv(data_path, sep="\t")
        .drop(["orgId", "sysName", "geneName", "desc"], axis=1)
        .set_index("locusId")
    )


def load_proteomics_data_gao2020(data_path, mapping_path):
    # These gene names here are not very consistent, I downloaded a mapping
    # from the protein name to the PP_xxxx idenfiers instead from uniprot.

    # protein_index = pd.read_excel(data_path, sheet_name="Supplementary Table 1")
    # protein_index = protein_index[["Gene Name", "Protein"]].set_index("Protein")

    gene_mapping = pd.read_csv(mapping_path, header=None, index_col=0)

    def read_abundance_data(sheet, gene_mapping):
        # Read in
        abundance = pd.read_excel(data_path, sheet_name=sheet, header=list(range(5)))
        # Correct column names and index
        column_levels = abundance.iloc[:, 0].name[:-2]
        abundance.columns = abundance.columns.droplevel(-1).droplevel(-1)
        abundance.columns.names = column_levels
        abundance = abundance.set_index(abundance.columns[0])
        abundance.index.name = "Protein Name"
        # Map to gene names
        abundance = abundance.assign(
            gene=abundance.index.to_series()
            .str.split("|", expand=True)[[1]]
            .replace(gene_mapping)
        ).set_index("gene")
        # Average replicas
        return abundance.groupby(["Sample Name", "Growth Medium"], axis=1).mean()

    nanoflow_abundance = read_abundance_data("Supplementary Table 6", gene_mapping)
    microflow_abundance = read_abundance_data("Supplementary Table 7", gene_mapping)
    return nanoflow_abundance, microflow_abundance


def load_proteomics_data_vasileva2018(data_path):
    df = pd.read_excel(
        data_path, sheet_name="All proteins (1592)", header=[2, 3], index_col=[0, 1, 2],
    ).iloc[:, :-3]
    df.index = df.index.droplevel(0).droplevel(1).str.lstrip("ppu:")
    df.columns = pd.MultiIndex.from_frame(
        (df.columns.to_frame().reset_index(drop=True).apply(lambda x: x.str.strip())),
        names=["experiment", "measure"],
    )

    # There are two duplicated rows.
    row1 = df.loc["PP_4421"].sum()
    row2 = df.loc["PP_4969"].sum()
    df = df.drop(["PP_4421", "PP_4969"])
    df.loc["PP_4421"] = row1
    df.loc["PP_4969"] = row2

    # exp 1 and 2 are biological duplicates
    # exp2 n1 and n2 are technical replicates
    # However, we will only use this for presence / absence
    df = df.groupby(lambda x: ("log" if "log" in x[0] else "stat", x[1]), axis=1).mean()
    df.columns = pd.MultiIndex.from_tuples(
        df.columns.to_list(), names=["experiment", "measure"]
    )
    df = df.loc[:, pd.IndexSlice[:, "Area"]] > 0
    df.columns = df.columns.droplevel(1)
    return df


def retrieve_evidence(genes):
    # Some info about wich genes to consider for normalization etc..
    go_graph_path = pathlib.Path("data/genome annotation/go-basic.obo")
    go_annotation_path = pathlib.Path(
        "data/genome annotation/Pseudomonas_putida_go_annotation_uniprot.tsv"
    )
    metabolic_genes_path = pathlib.Path("data/genome annotation/metabolic_from_go.csv")
    if metabolic_genes_path.exists():
        metabolic_genes = pd.read_csv(metabolic_genes_path, index_col=0)
    else:
        metabolic_genes = get_metabolic_genes(go_annotation_path, go_graph_path)
        metabolic_genes.to_csv(metabolic_genes_path)

    # Transcriptomics data
    genome_path = pathlib.Path("data/genome annotation/pp_kt2440_AE015451_1.gb")
    transcriptomics_data_path = pathlib.Path("data/studies/Kim2013/")
    transcriptomics_df = load_transcriptomics_data_kim2013(
        transcriptomics_data_path, genome_path
    )
    transcriptomics_df = transcriptomics_df.loc[
        metabolic_genes.reindex(transcriptomics_df.index).fillna(False).metabolic
    ]

    transcriptomics_data_path_2 = pathlib.Path(
        "data/studies/Sudarsan2014/GSE26785_FullNormalizedMatrix.txt"
    )
    transcriptomics_df_2 = load_transcriptomics_data_sudarsan2014(
        transcriptomics_data_path_2
    )
    transcriptomics_df_2 = transcriptomics_df_2.loc[
        metabolic_genes.reindex(transcriptomics_df_2.index).fillna(False).metabolic
    ]

    transcriptomics_data_path_3 = pathlib.Path("data/studies/DArrigo2019/GSE118554_RAW")
    transcriptomics_df_3 = load_transcriptomics_data_d_arrigo2019(
        transcriptomics_data_path_3
    )
    transcriptomics_df_3 = transcriptomics_df_3.loc[
        metabolic_genes.reindex(transcriptomics_df_3.index).fillna(False).metabolic
    ]

    # Reaction reversibility data
    reversibility_path = pathlib.Path(
        "./data/models/Nogales2019/reversibility_conflicts.xlsx"
    )
    thermodynamics_df = pd.read_excel(reversibility_path, "reactions").set_index(
        "reaction"
    )

    # Essentiality data
    essentiality_path = pathlib.Path(
        "data/studies/Banerjee2020/41467_2020_19171_MOESM6_ESM.xlsx"
    )
    essentiality_df = load_essentiality_data_banerjee2020(essentiality_path)
    essentiality_path_2 = pathlib.Path(
        "data/studies/Thompson2019/fit_organism_Putida.tsv"
    )
    essentiality_df_2 = load_essentiality_data_fitness_browser(essentiality_path_2)

    # Proteomics data
    gao_data_path = pathlib.Path(
        "data/studies/Gao2020/Table_1_High-Throughput Large-Scale Targeted "
        "Proteomics Assays for Quantifying Pathway Proteins in Pseudomonas putida KT2440.xlsx"
    )
    gao_mapping_path = pathlib.Path("data/studies/Gao2020/gene_mapping.csv")
    gao_nano, gao_micro = load_proteomics_data_gao2020(gao_data_path, gao_mapping_path)

    vasileva_data_path = pathlib.Path(
        "data/studies/Vasileva2018/emi412639-sup-0002-suppinfo2.xlsx"
    )
    vasileva_df = load_proteomics_data_vasileva2018(vasileva_data_path)

    # Gather evidence per gene.
    evidence_per_gene = collections.defaultdict(list)
    # Banjerjee 2020 - only contains essential genes.
    for gene in essentiality_df.index:
        evidence_per_gene[gene].append(
            ("positive", "essentiality", "RB-TnSeq", "", "Banerjee2020")
        )
    # Fitness Browser
    # "In general, if -1 < fitness < 1, then the gene has a subtle phenotype that might be
    #  statistically significant (see t scores) but will probably be difficult to interpret.
    #  Fitness < -2 or fitness > 2 are strong fitness effects."
    for gene in essentiality_df_2.index:
        score = essentiality_df_2.loc[gene].mean()
        if -1 < score < 1:
            indication = "neutral"
        elif score > 2:
            indication = "positive"
        else:
            indication = "negative"
        evidence_per_gene[gene].append(
            (indication, "essentiality", "RB-TnSeq", score, "Fitness Browser")
        )

    ref_ratio = 0.2
    ref_pct = 0.2

    # Transcriptomics data
    # Kim 2013 /  D'Arrigo 2019 / Sundarsan2014
    transcriptomics_data = [
        (
            transcriptomics_df,
            "glucose",
            ["succinate", "fructose", "glycerol"],
            "Kim2013",
        ),
        (
            transcriptomics_df_2,
            "glucose",
            ["fructose", "benzoate I", "benzoate II"],
            "Sudarsan2014",
        ),
        (transcriptomics_df_3, "Glu", ["Cit", "Fer", "Ser"], "D'Arrigo2019"),
    ]
    for df, ref_column, other_columns, source in transcriptomics_data:
        # Check relative expression level on glucose compared to other genes
        for gene, pct in df[ref_column].rank(pct=True).items():
            evidence_per_gene[gene].append(
                (
                    "negative" if pct < ref_pct else "positive",
                    "expression",
                    "percentile (glucose)",
                    pct,
                    source,
                )
            )
        # Check relative expression on glucose (vs other carbon-sources)
        expression_ratio = (df[ref_column] / df[other_columns].T).T
        for gene in expression_ratio.index:
            for other_condition in expression_ratio.columns:
                ratio = expression_ratio.loc[gene, other_condition]
                if pd.isna(ratio) or ratio == 0:
                    continue
                evidence_per_gene[gene].append(
                    (
                        "negative" if ratio < ref_ratio else "positive",
                        "expression",
                        "relative ({})".format(other_condition),
                        ratio,
                        source,
                    )
                )

    # Gao 2020
    for df, experiment in zip((gao_nano, gao_micro), ("nanoflow", "microflow")):
        # Check relative expression level on glucose compared to other genes
        df_mean = df.groupby(["Sample Name"], axis=1).mean()

        for gene, pct in df_mean.Glucose.rank(pct=True).items():
            evidence_per_gene[gene].append(
                (
                    "negative" if pct < ref_pct else "positive",
                    "protein abundance",
                    "percentile (glucose, {})".format(experiment),
                    pct,
                    "Gao2020",
                )
            )

        # Check relative expression on glucose (vs Coumarate, Fructose, Gluconate)
        # We're not using the mixed carbon source data here.
        expression_ratio = (
            df_mean.Glucose / df_mean[["Coumarate", "Fructose", "Gluconate"]].T
        ).T
        for gene in expression_ratio.index:
            for other_condition in expression_ratio.columns:
                ratio = expression_ratio.loc[gene, other_condition]
                evidence_per_gene[gene].append(
                    (
                        "negative" if ratio < ref_ratio else "positive",
                        "protein abundance",
                        "relative ({}, {})".format(other_condition, experiment),
                        ratio,
                        "Gao2020",
                    )
                )

    # Vasileva 2018
    for gene in vasileva_df.index:
        for experiment in vasileva_df.columns:
            present = vasileva_df.at[gene, experiment]
            evidence_per_gene[gene].append(
                (
                    "positive" if present else "negative",
                    "protein presence",
                    "{} phase".format(experiment),
                    present,
                    "Vasileva2018",
                )
            )
    # Add genes not found in this dataset as neutral evidence.
    for gene in set(genes) - set(vasileva_df.index):
        evidence_per_gene[gene].append(
            ("neutral", "protein presence", "not detected", 0.0, "Vasileva2018",)
        )

    confidence_map = {
        0: "N/A",
        # 1 - In silico evidence (e.g. gapfilling)
        1: "in silico",
        # 2 - Genomic or physiological evidence
        2: "genomic / physiological",
        # 3 - Genetic evidence such as knock-outs
        3: "genetic",
        # 4 - Complete characeterization of the GPR
        4: "characterized",
    }
    # Gather evidence per reaction.
    # (reaction_id, gene_id, required/optional gene, neutral/negative/postive,
    #  type, subtype, score, source)
    evidence_per_reaction = []
    for reaction in model.reactions:
        # Check confidence scores
        score = confidence.Confidence.get(reaction.id, 0)
        if score < 2:
            indication = "negative"
        elif score > 2:
            indication = "positive"
        else:
            indication = "neutral"
        evidence_per_reaction.append(
            (
                reaction.id,
                "",
                "",
                indication,
                "confidence",
                confidence_map[score],
                score,
                "Nogales2019",
            )
        )
        # Check reversibility matches
        try:
            thermodynamics = thermodynamics_df.loc[reaction.id]
        except KeyError:
            pass
        else:
            if thermodynamics.status == "incomplete":
                evidence_per_reaction.append(
                    (
                        reaction.id,
                        "",
                        "",
                        "neutral",
                        "thermodynamics",
                        "incomplete",
                        thermodynamics.ln_rev_index,
                        "eQuilibrator",
                    )
                )
            elif thermodynamics.status == "transport":
                evidence_per_reaction.append(
                    (
                        reaction.id,
                        "",
                        "",
                        "positive",
                        "thermodynamics",
                        "transport",
                        thermodynamics.ln_rev_index,
                        "eQuilibrator",
                    )
                )
            elif (
                thermodynamics.model_reversibility
                == thermodynamics.equilibrator_reversibility
            ):
                evidence_per_reaction.append(
                    (
                        reaction.id,
                        "",
                        "",
                        "positive",
                        "thermodynamics",
                        "reversibility match",
                        thermodynamics.ln_rev_index,
                        "eQuilibrator",
                    )
                )
            else:
                evidence_per_reaction.append(
                    (
                        reaction.id,
                        "",
                        "",
                        "negative",
                        "thermodynamics",
                        "reversibility mismatch",
                        thermodynamics.ln_rev_index,
                        "eQuilibrator",
                    )
                )

        # Based on expression of related genes.
        tree, genes = cobra.core.gene.parse_gpr(reaction.gene_reaction_rule)
        for gene in genes:
            required = not cobra.core.gene.eval_gpr(tree, {gene})
            for evidence in evidence_per_gene[gene]:
                evidence_per_reaction.append(
                    (reaction.id, gene, "required" if required else "optional")
                    + evidence
                )

    evidence = pd.DataFrame(
        evidence_per_reaction,
        columns=[
            "reaction",
            "gene",
            "required",
            "status",
            "type",
            "specification",
            "score",
            "source",
        ],
    )
    return evidence


if __name__ == "__main__":
    model = cobra.io.read_sbml_model("./data/models/Nogales2019/Bigg_iJN1463.xml")

    pdh_reactions = pdh_reactions
    biomass = biomass_reaction

    # Confidence score (Nogales et al. 2019)
    # 1 - In silico evidence (e.g. gapfilling)
    # 2 - Genomic or physiological evidence
    # 3 - Genetic evidence such as knock-outs
    # 4 - Complete characeterization of the GPR
    mapping_path = pathlib.Path("data/models/Nogales2019/nogales_bigg_mapping.json")
    confidence_data_path = pathlib.Path(
        "./data/models/Nogales2019/emi14843-sup-0004-tables1.xlsx"
    )
    confidence, subsystem = get_nogales_data(confidence_data_path, mapping_path)
    # Reaction cost is mapped a bit different, as score 3 and 4 are equally good for our purpose.
    # In silico / N.A. -> 1
    # Genomic / physiological -> 1.5
    # Knockout / characterized -> 2.0
    reaction_cost = confidence.Confidence.map(
        {0: 1, 1: 1, 2: 1.5, 3: 2, 4: 2}
    ).to_dict()

    # In addition to the certainty, include the graph distance to coa
    # Note that the distance is reactions + metabolites
    # We add a cost of distance / 10
    # example: pdh (irrev) -> accoa -> ACOATA -> coa_c (distance 3, cost 3/10=.3)
    ignore_metabolites = {
        "ACP_c",
        "adp_c",
        "amp_c",
        "atp_c",
        "fad_c",
        "fadh2_c",
        "q8_c",
        "q8h2_c",
        "nad_c",
        "nadh_c",
        "nadp_c",
        "nadph_c",
    } | {m.id for m in model.metabolites if re.match(".*[CRX].*", m.formula) is None}

    distance = get_graph_distance_to(model, "coa_c", ignore_metabolites)
    for reaction in reaction_cost:
        reaction_cost[reaction] += distance.get(reaction, 10) / 10

    # Remove some reactions that are basically duplicates
    # This will make the search easier, since it won't require finding both of them at the same time.
    # Entries were generated by: find_similar_reactions + manual curation
    # Usually the reversible variant, or the variant using nadh instead of nadph is kept.
    # It is in the format:
    #   reaction kept: [reaction removed, ...]
    # This way you can easily check the final list of removed reactions in the produced
    # strain to see which alternative reactions should be considered for removal as well.
    duplicate_reactions = {
        "4OXPTCOADHx": ["4OXPTCOADHy"],
        "ACACT1r": ["KAT1"],
        "AKGDb_copy1": ["AKGDb_copy2"],
        "ALDD2x": ["ALDD2y"],
        "ALDD3y_copy2": ["ALDD3y_copy1"],
        "ARGDI": ["ARGDr"],
        "BETALDHx": ["BETALDHy"],
        "CRO4Rx": ["CRO4Ry"],
        "DHAD1_copy1": ["DHAD1_copy2"],
        "DHMPTRx": ["DHMPTR"],
        "DHQTi_copy1": ["DHQTi_copy2"],
        "EAR121x": ["EAR121y"],
        "FMNRx2_copy1": ["FMNRx", "FMNRx2_copy2"],
        "FRDO6r": ["FRDO5r"],
        "FRDO7r": ["FRDO4r"],
        "G3PD2": ["G3PD5"],
        "G5SD_copy2": ["G5SD_copy1"],
        "GAPD": ["GAPDi_nadp"],
        "GLNS_copy1": ["GLNS_copy2"],
        "GLUCYS_copy2": ["GLUCYS_copy1"],
        "GLUDxi": ["GLUDy"],
        "GLYCLTDx": ["GLYCLTDy"],
        "HISTD": ["HDH"],
        "HMR_0180_copy2": ["HMR_0180_copy1"],
        "HPYRRx": ["HPYRRy"],
        "LDH_D": ["LDH_D2"],
        "MDH": ["DMALRED", "MDH2"],
        "MECDPS_copy1": ["MECDPS_copy2"],
        "MEPCT": ["MEPCT_1"],
        "NAt3pp_copy1": ["NAt3pp_copy2"],
        "NODOx": ["NODOy"],
        "PRAIi_copy1": ["PRAIi_copy2"],
        "PRAMPC_copy2": ["PRAMPC_copy1"],
        "PRASCSi_copy1": ["PRASCSi_copy2"],
        "PRMICI_copy1": ["PRMICI_copy2"],
        "PSCVT_copy1": ["PSCVT_copy2"],
        "PTPATi": ["APPAT"],
        "SERAT_copy1": ["SERAT_copy2"],
        "SSALx": ["SSALy"],
        "TNTR1x": ["TNTR1"],
        "TNTR2x": ["TNTR2"],
        "TRPS1_copy1": ["TRPS1_copy2"],
        "UKQUIVORx": ["UKQUIVORy"],
        "VALTRS_copy1": ["VALTRS_copy2"],
    }
    reactions_to_remove = sorted(
        itertools.chain.from_iterable(duplicate_reactions.values())
    )
    # for keep, remove in duplicate_reactions.items():
    #     print(model.reactions.get_by_id(keep))
    #     for r in remove:
    #         print('\t', model.reactions.get_by_id(r))
    #     print()

    # Update the model to remove the duplicates and add free co-factors.
    simplified_model, free_cofactor_reactions = update_model(model, reactions_to_remove)

    # Check essentiality for all conditions we tested experimentally.
    essential = find_essential_reactions(
        model,
        itertools.chain(growth_conditions.values(), production_strains.values()),
        0.9,
        0.9,
    )

    ignore = {
        # Some core reactions we can ignore.
        "EDA",
        "EDD",
        "ENO",
        "FUM",
        "GAPD",
        "ICL",
        "MDH",
        "PC",
        "PGL",
        "PPC",
        "SUCOAS",
        "R1PK",
        "HEX1",
        # We don't care about cofactor exchange reactions.
        "ADK1",
        "ADK3",
        "ATPM",
        "NADH5",
        "NADK",
        "NADPHQR2",
        "NADTRHD",
        "NDPK1",
        "NTP1",
        "SUCD4",
        "ADK1",
        "ADK3",
        "IDPh_1",
        "NADH5",
        "NADK",
        "NDPK1",
        "NTP1",
        "PPA",
        "SUCD4",
        # Model growth reactions
        "BIOMASS_KT2440_Core2",
        "BIOMASS_KT2440_WT3",
        "ATPM",
    }

    reactions = [
        r
        for r in simplified_model.reactions
        # Only if the reaction is cytosolic
        if r.compartments == {"c"}
        # and not on the ignore list
        and r.id not in ignore
        # and not essential in the targeted conditions
        and r not in essential
        # and not a transporter, sink, exchange etc.
        and not re.search(r"^(?:DM|EX|free)|(?:pp|ppi|ex|exi|abc)$", r.id)
    ]

    ko_results_path = pathlib.Path("results/ko_strains.txt")
    run_ko_prediction = False
    biomass_threshold = 0.1
    if run_ko_prediction and not ko_results_path.exists():
        success = find_required_ko(
            model=simplified_model,
            objective_reaction=simplified_model.reactions.get_by_id("EX_accoa_c"),
            biomass_reaction=simplified_model.reactions.get_by_id(biomass),
            essential_reaction=simplified_model.reactions.get_by_id("PDH"),
            starting_strain=(("PDH", 1), ("PDHa", 1), ("PDHbr", 1), ("PDHbr", -1)),
            allowed_reactions=reactions,
            free_cofactor_reactions=free_cofactor_reactions,
            reaction_cost=reaction_cost,
            results_path=ko_results_path,
            max_length=12,
            length_penalty=0.1,
            objective_threshold=1,
            biomass_threshold=biomass_threshold,
            flux_threshold=1e-6,
            verbose=True,
        )

    # Retrieve evidence per reaction / gene.
    evidence = retrieve_evidence([i.id for i in model.genes]).sort_values(
        ["reaction", "required", "gene"]
    )

    # Read in successes
    success = [line.split(", ") for line in ko_results_path.read_text().splitlines()]
    success_by_length = collections.defaultdict(list)
    for i in success:
        i = sorted(set(i) - set(pdh_reactions))
        success_by_length[len(i)].append(i)

    print(
        "Found {} possible ko combinations for PDH essential phenotype.".format(
            len(success)
        )
    )
    for length, combinations in success_by_length.items():
        print("  length {}: {:>4} combinations".format(length, len(combinations)))

    # Retrieve the complete solution for all of the shortest solutions,
    # and add back duplicate reactions where required.
    limited = success_by_length[6]
    full_model_success = [
        augment_solution_with_duplicates(
            model, duplicate_reactions, strain, biomass_threshold
        )
        for strain in limited
    ]

    # It's easier to look at the knockouts as a tree.
    # print_as_tree([list(i) + ['-'] for i in success])

    prevalence = dict(
        collections.Counter(itertools.chain.from_iterable(success)).most_common()
    )
    reaction_count = pd.DataFrame(
        {
            k: (v, subsystem.get(get_reaction_and_direction(k)[0], "Unannotated"))
            for k, v in collections.Counter(
                itertools.chain.from_iterable(success)
            ).items()
        },
        index=["count", "subsystem"],
    ).T.sort_values("count", ascending=False)
    subsystem_prevalence = (
        reaction_count.groupby("subsystem")
        .count()
        .sort_values("count", ascending=False)
    )

    print("Top 10 most common reactions")
    print("Reaction     | count")
    print("--------------------")
    for reaction, count in sorted(prevalence.items(), key=lambda x: x[1], reverse=True)[
        :10
    ]:
        print(
            "{:<12} | {:>4} ({:.1f}%)".format(
                reaction, count, 100.0 * count / len(success)
            )
        )

    # Organize evidence.
    # Status:
    #   Negative => Evidence for reaction being missing/inactive
    #   Neutral  => Not enough evidence for either
    #   Positive => Evidence for reaction being present/active
    # Required:
    #  Whether the gene is
    #    (1) required (AND) or
    #    (2) one of multiple variants (OR)
    #  for a reaction according to the GPR rule in iJN1463.
    evidence_counts = (
        evidence.groupby(["reaction", "status", "type"])
        .gene.count()
        .unstack([-1, -2])
        .fillna(0)
        .astype(int)
        .sort_index(1)
    )

    # Check evidence / phenotype per combination.
    data = []
    combinations = []
    for combination in full_model_success:
        combination_non_directional = [
            get_reaction_and_direction(reaction)[0] for reaction in combination
        ]
        e = evidence_counts.loc[combination_non_directional].clip(0, 1)
        totals = (
            e.loc[:, :]
            .groupby(level="status", axis=1)
            .any()
            .sum()
            .reset_index()
            .assign(type="total")
            .set_index(["type", "status"])
        )
        growth = pd.DataFrame(
            verify_in_full_model(model, combination),
            index=["ref", "no_pdh", "ko", "repair"],
            columns=["growth"],
        ).unstack()
        entry = pd.concat([e.sum(), totals, growth])
        entry.loc[pd.IndexSlice["length", ""], 0] = len(combination)
        # Verify the growth phenotype (this is a bit slow, so you might want to leave it out.)
        combinations.append(
            tuple(sorted(combination, key=lambda x: prevalence.get(x, 0), reverse=True))
        )
        data.append(entry)
    combination_evidence = (
        pd.concat(data, axis=1)
        .T.set_index(pd.MultiIndex.from_frame(pd.DataFrame(combinations)))
        .sort_values(("total", "negative"), ascending=False)
    )

    all_combinations = pd.DataFrame(
        [
            tuple(sorted(combination, key=lambda x: prevalence.get(x, 0), reverse=True))
            for combination in success
        ]
    ).sort_values(
        by=[0, 1, 2, 3, 4, 5, 6],
        key=lambda x: [prevalence.get(i, 0) for i in x],
        ascending=False,
    )

    # Save all combinations and evidence
    with pd.ExcelWriter("results/combinations.xlsx") as writer:
        combination_evidence.to_excel(writer, sheet_name="shortest")
        all_combinations.to_excel(writer, sheet_name="all")
        reaction_count.to_excel(writer, sheet_name="reaction_counts")
        evidence.to_excel(writer, sheet_name="evidence")

    # Print top combinations
    best_evidence = combination_evidence[
        (
            combination_evidence.total.negative
            == combination_evidence.total.negative.max()
        )
        & (combination_evidence.growth.repair > 1e-3)
    ]
